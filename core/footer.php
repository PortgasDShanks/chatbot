<footer class="main-footer">
    <div class="float-right d-none d-sm-inline">
      ~ Red-Haired Pirates
    </div>
    <strong>Copyright &copy; <?php echo date("Y") ?> .</strong> All rights reserved.
  </footer>