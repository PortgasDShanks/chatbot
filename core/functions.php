<?php
function clean($str) {
    $str = @trim($str);
    if(get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return mysql_real_escape_string($str);
}
function getCurrentDate(){
	ini_set('date.timezone','UTC');
	date_default_timezone_set('UTC');
	$today = date('H:i:s');
	$date = date('Y-m-d H:i:s', strtotime($today)+28800);
	
	return $date;
}

function select_query($type , $table , $params){

	$select_query = mysql_query("SELECT $type FROM $table WHERE $params")or die(mysql_error());
	$fetch = mysql_fetch_assoc($select_query);
	return $fetch;

}

function checkSession(){
	if(!isset($_SESSION['user_id'])){
	header("Location: index.php");
	}
}

function insert_query($table_name, $form_data , $last_id){
    $fields = array_keys($form_data);

    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";

    $return_insert = mysql_query($sql)or die(mysql_error());
    $lastID = mysql_insert_id();

    if($last_id == 'Y'){
        if($return_insert){
            echo $lastID;
        }else{
            echo 0;
        }
    }else{
        if($return_insert){
            echo 1;
        }else{
            echo 0;
        }
    }

    
}

function delete_query($table_name, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "DELETE FROM ".$table_name.$whereSQL;
    
    $return_delete = mysql_query($sql);

    if($return_delete){
        echo 1;
    }else{
        echo 0;
    }
    
    
}

function update_query($table_name, $form_data, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "UPDATE ".$table_name." SET ";
    $sets = array();
    foreach($form_data as $column => $value)
    {
         $sets[] = "`".$column."` = '".$value."'";
    }
    $sql .= implode(', ', $sets);
    $sql .= $whereSQL;

    $return_query = mysql_query($sql) or die(mysql_error());
    if($return_query){
    	echo 1;
    }else{
    	echo 0;
    }
}
function split_words($string, $nb_caracs, $separator){
    $string = strip_tags(html_entity_decode($string));
    if( strlen($string) <= $nb_caracs ){
        $final_string = $string;
    } else {
        $final_string = "";
        $words = explode(" ", $string);
        foreach( $words as $value ){
            if( strlen($final_string . " " . $value) < $nb_caracs ){
                if( !empty($final_string) ) $final_string .= " ";
                $final_string .= $value;
            } else {
                break;
            }
        }
        $final_string .= "<p data-toggle='tooltip' data-placement='top' title='".$string."'>" . $separator . "<p/>";
    }
    return $final_string;
}
function getPosition($project_id , $member_id){

    $get_memberPosition = mysql_fetch_array(mysql_query("SELECT * FROM pms_project_members as pm , pms_users as u WHERE pm.project_member_id = u.user_id AND pm.project_id = '$project_id' AND project_member_id = '$member_id'"));
        if($get_memberPosition['project_access'] == 'P'){
          $position = 'Team Leader/Project Manager';
        }else if($get_memberPosition['project_access'] == 'M'){
          $position = 'Project Member';
        }else{
          $position = '';
        }
        return $position; 
}

function countMembers($project_id){
    $get_allMembers = mysql_query("SELECT * FROM pms_project_members as pm , pms_users as u WHERE pm.project_member_id = u.user_id AND project_id = '$project_id' AND pm.project_access != 'P'");
    $count_members = mysql_num_rows($get_allMembers);

    return $count_members;
}

function getProjectLeader($project_id){
    $get_leader = mysql_fetch_array(mysql_query("SELECT * FROM pms_project_members as pm , pms_users as u WHERE pm.project_member_id = u.user_id AND project_id = '$project_id' AND pm.project_access = 'P'"));

    return $get_leader; 
}
function getUser($user_id){
    $getUser = mysql_fetch_array(mysql_query("SELECT username as fullname FROM tbl_users WHERE user_id = '$user_id'"));

    return $getUser[0];
}

function countAllOnline_groupChats($gcID){
    $countOnlineUsers = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_users as u LEFT JOIN tbl_group_chat_members as m on u.user_id = m.member_id LEFT JOIN tbl_group_chats as c on c.group_chat_id = m.group_chat_id WHERE u.user_online_status = 1 AND c.group_chat_id = '$gcID'"));

    return $countOnlineUsers[0];
}
function getAllUsers($session_user_id){
    $content = "<option value=''> -- Please Choose Members -- </option>";
    $users = mysql_query("SELECT CONCAT(user_firstname,' ',user_lastname) as fname , user_id as uID FROM tbl_users WHERE user_id != '$session_user_id'");
    while($row = mysql_fetch_array($users)){
        $content .= "<option value='".$row['uID']."'>".$row['fname']."</option>";
    }

    return $content;
}
function GETALLCATEGORIES_list(){
    $content = "<option value=''> &mdash; Please Choose Category &mdash; </option>";
    $cat = mysql_query("SELECT * FROM tbl_chat_categories");
    while($row = mysql_fetch_array($cat)){
        $content .= "<option value'".$row['category_id']."'>".$row['category_name']."</option>";
    }

    return $content;
}
function getGuestName($convoID){
    $name = mysql_fetch_array(mysql_query("SELECT guest_name FROM tbl_conversation_header WHERE conversation_header_id = '$convoID'"));

    return $name[0];
}
function getConvoCategory($convoID){
    $cat = mysql_fetch_array(mysql_query("SELECT * FROM tbl_conversation_categories WHERE convo_header_id = '$convoID'"));

    return $cat['category_id'];
}
function getTimeLapse($dateNow, $dateNotif){
    $time1 = new DateTime($dateNow);
    $time2 = new DateTime($dateNotif);
    $diff = $time->diff($time2);
    $minutes = ($diff->days * 24 * 60) +
               ($diff->h * 60) + $diff->i;

    return $minutes;
}
function getKeywords_from_msg($chatID){
    $getMessage = mysql_fetch_array(mysql_query("SELECT msg_content FROM tbl_conversation_detail WHERE conversation_detail_id = '$chatID'"));

    $msgs_explode = explode(" ",$getMessage[0]);
    $result = array();
    foreach($msgs_explode as $msgs){
        if(strlen($msgs) >= 4){
            $result[] = $msgs;
        }
    }

    return $result;
}
function getkeywordAnswer($keywords,$category){
    
    if(count($keywords) > 0){
        $keywordIDs = array();
        $keyExist = array();
        foreach($keywords as $chars){
            $k = mysql_query("SELECT * FROM tbl_category_keywords WHERE FIND_IN_SET('$chars',keywords) AND category_type = '$category'");
            while($row = mysql_fetch_array($k)){
                if(!empty($row['keyword_id'])){
                    if(in_array($row['keyword_id'],$keyExist)){
                        $keywordIDs[$row['keyword_id']] += 1;
                    }else{
                        $keywordIDs[$row['keyword_id']] = 1;
                    }
                    $keyExist[] = $row['keyword_id'];
                }
            }
           
        }
    }
    return $keywordIDs;
}

function keywords_get_count($array){
    $c = array();
    if(count($array) > 0){
        foreach($array as $a => $b){
            $c[] = $b;
        }
    }
    return $c;
}
function keywords_check_if_unique($array1,$array2){
    $d = array();
    $count = count($array1);
    if(count($array1) > 1){
        $a = max($array1);
        foreach($array2 as $b => $c){
            if($a == $c){
                $d[] =  $b;
            }
        }
    }else{
        foreach($array2 as $b => $c){
            $d[] =  $b;
        }
    }

    return $d;
}
function keywords_get_highest_count(){

}
function keyword_get_highest_if_multiple($array){
    $max = 0;
    if(count($array) > 0){
        foreach( $array as $k => $v )
        {
            $max = max( array( $max, $v ) );
        }
    }
    return $max;
}
function if_question_is_general($array){
    arsort($array);
    if(count($array) > 0){
        foreach($array as $a => $b){
            $keyID[] = $a;
        }
    }
    return $keyID;
}
function CHATBOTUSERS($cat){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $where = ($cat == 'today')?" WHERE date_added = '$curdate'":"";
    $query = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_conversation_header $where "));

    return $query[0];
}
function GETBOTREPLY($convoID){
    $content = "";
    $query = mysql_fetch_array(mysql_query("SELECT * FROM tbl_conversation_detail WHERE conversation_detail_id = '$convoID'"));
    $msg_content = $query['msg_content'];
    $is_multiple = $query['msg_is_multiple'];
    if($is_multiple == 0){
       $content .= "<p>".$query['msg_content']."</p>";
    }else if($is_multiple == 1){
        $getText = mysql_fetch_array(mysql_query("SELECT keyword_answers , additional_info , answer_id FROM tbl_keywords_answers WHERE keyword_id = '$msg_content'"));
        

        $content = "<p style='cursor: help;' onclick='showInfo(".$getText[2].")'>".$getText[0]."</p>";
    }else{
        $explode = explode(",", $msg_content);
        foreach($explode as $kID){
            $getText = mysql_fetch_array(mysql_query("SELECT keyword_answers , additional_info , answer_id FROM tbl_keywords_answers WHERE keyword_id = '$kID'"));
            $cursor = ($getText[1] == '')?"":"cursor: help";
            $content .= "<p style='".$cursor."' onclick='showInfo(".$getText[2].")'>".$getText[0]."</p>";
        }
    }

    return $content;
}
?>