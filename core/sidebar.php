<?php
require 'sidebar_control.php';
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="#" class="brand-link">
      <img src="assets/images/messages.png" alt="No Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text"><strong>CHATBOT</strong></span>
    </a>
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
         <img src="assets/images/avatar.png" class="img-circle" style="height: 50px; width: 50px; border-radius: 50%; object-fit: cover;" alt="User Image">
        </div>
        <div class="info" style="padding: 13px;">
          <a href="#" class="d-block"><?php echo getUser($user_id)?></a>
        </div>
      </div>
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" style="margin-top: 5px;">
          <li class="nav-item has-treeview">
            <a href="index_1.php?page=dashboard" class="nav-link <?php echo $dash; ?>">
              <i class="nav-icon fa fa-dashboard" style="font-size: 15px;"></i>
              <p style="font-size: 16px;">
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="index_1.php?page=categories" class="nav-link <?php echo $cat; ?>">
              <i class="nav-icon fa fa-list" style="font-size: 15px;"></i>
              <p style="font-size: 16px;">
                Categories
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="index_1.php?page=QandA" class="nav-link <?php echo $QA; ?>">
              <i class="nav-icon fa fa-question-circle" style="font-size: 15px;"></i>
              <p style="font-size: 16px;">
                Q&A
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>