<style type="text/css">
  .control-sidebar-dark, .control-sidebar-dark:before {
    background: #f4f6f9;
  }
  .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #8e8d8d;
    background-color: #ffffff;
  }
  /* .nav-item{
    color: white;
    text-shadow: 2px 2px 4px #000000;
  } */
  .control-sidebar-dark, .control-sidebar-dark:before {
   background: rgba(52, 57, 64, 0.7);
  }
  .img-size-50 {
    width: 25px;
  }
  /*.media:hover{
    background-color: white;
    border-radius: 5px 5px 5px 5px;
  }*/
  .text_content:hover{
    text-decoration: underline;
    cursor: pointer;
  }
  .notif{
    list-style: none;
  }
  #icon_hover{
    cursor: pointer;
  }
  #star_:hover{
    background-color: #525458;
    border-radius: 50%;
    cursor: pointer;
  }
  #see_all:hover{
    cursor: pointer;
    background-color: #71747b;
    border-radius: 5px;

  }
</style>
<aside class="control-sidebar control-sidebar-dark">
    <ul class="nav nav-pills" style="padding-left: 50px;padding-top: 10px;">
      <li class="nav-item"><a class="nav-link active show" href="#tab_notifs" data-toggle="tab"><span class="fa fa-bell"></span></a></li>
      <li class="nav-item"><a class="nav-link" href="#tab_message" data-toggle="tab"><span class="fa fa-envelope"></span></a></li>
      <li class="nav-item"><a class="nav-link" href="#tab_settings" data-toggle="tab"><span class="fa fa-cogs"></span></a></li>
    </ul>
              
             
    <div class="tab-content">
      <div class="tab-pane active show" id="tab_notifs">
          <center><h6 style="color: #6b737b;background-color: #fff;border-radius: 5px 5px 5px 5px;padding: 5px;">Today</h6></center>
          <hr>
          <?php
            echo "<h2>".date("l").", <br>".date("jS")." ".date("F")."</h2>";
          ?>
          <div class="card">
              <div class="card-header" style="background-color: #84797961;">
               <span class="fa fa-calendar" style="color: #6c757d;"></span><h6 style="color: #6c757d;display: inline;"> Calendar</h6>
              </div>
              <div class="card-body">
                <p class="card-text" style="color: #6c757d;">No events for today.</p>
              </div>
            </div>
            <div class="card">
              <div class="card-header" style="background-color: #84797961;">
               <span class="fa fa-bell" style="color: #6c757d;"></span><h6 style="color: #6c757d;display: inline;"> Reminders</h6>
              </div>
              <div class="card-body">
                <p class="card-text" style="color: #6c757d;">No Reminders</p>
              </div>
            </div>
      </div>
      <div class="tab-pane" id="tab_message">
          
      </div>
      <div class="tab-pane" id="tab_settings">
        <!-- (profile , logout , add etc.) -->
        <h6 style="font-size: 12px;color: #ffffff;">Settings Navigation</h6>
         <ul style="list-style: none;padding:5px;cursor: pointer;">
           <li onclick="logout()" class="liHover"><span class="fa fa-power-off"></span> Logout</li>
         </ul>
      </div>
    </div>
</aside>
<script type="text/javascript">
  function goto_messages(){
    window.location = "index.php?page=messages";
  }
  function showMenus(){
    $("#menu_div").slideToggle();
  }
</script>