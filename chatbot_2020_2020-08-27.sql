# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: chatbot_2020
# Generation Time: 2020-08-26 23:52:15 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_category_keywords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_category_keywords`;

CREATE TABLE `tbl_category_keywords` (
  `keyword_id` int(11) NOT NULL AUTO_INCREMENT,
  `keywords` text NOT NULL,
  `category_type` int(11) NOT NULL,
  PRIMARY KEY (`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_chat_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_chat_categories`;

CREATE TABLE `tbl_chat_categories` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_chat_categories` WRITE;
/*!40000 ALTER TABLE `tbl_chat_categories` DISABLE KEYS */;

INSERT INTO `tbl_chat_categories` (`category_id`, `category_name`)
VALUES
	(1,'CHMSC Personnel'),
	(2,'Enrollment Flow'),
	(3,'CHMSC History');

/*!40000 ALTER TABLE `tbl_chat_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_chat_notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_chat_notifications`;

CREATE TABLE `tbl_chat_notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_id` int(11) NOT NULL,
  `notification_datetime` datetime NOT NULL,
  `notification_type` varchar(50) NOT NULL DEFAULT '',
  `notification_status` int(1) NOT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_chat_notifications` WRITE;
/*!40000 ALTER TABLE `tbl_chat_notifications` DISABLE KEYS */;

INSERT INTO `tbl_chat_notifications` (`notification_id`, `chat_id`, `notification_datetime`, `notification_type`, `notification_status`)
VALUES
	(10,65,'2020-08-17 16:05:56','Message',1),
	(11,67,'2020-08-17 16:06:18','Message',0);

/*!40000 ALTER TABLE `tbl_chat_notifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_conversation_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_conversation_categories`;

CREATE TABLE `tbl_conversation_categories` (
  `convo_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `convo_header_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`convo_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_conversation_categories` WRITE;
/*!40000 ALTER TABLE `tbl_conversation_categories` DISABLE KEYS */;

INSERT INTO `tbl_conversation_categories` (`convo_category_id`, `convo_header_id`, `category_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(3,1,3),
	(4,2,1),
	(5,3,1),
	(6,3,2),
	(7,4,1),
	(8,5,1),
	(9,5,3),
	(10,5,2),
	(11,6,1),
	(12,7,1);

/*!40000 ALTER TABLE `tbl_conversation_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_conversation_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_conversation_detail`;

CREATE TABLE `tbl_conversation_detail` (
  `conversation_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `convo_category_id` int(11) NOT NULL,
  `sender_name` varchar(50) NOT NULL,
  `receiver_name` varchar(50) NOT NULL,
  `msg_content` text NOT NULL,
  `msg_datetime` datetime NOT NULL,
  `msg_is_answered` int(1) NOT NULL,
  `msg_is_bot` int(1) NOT NULL,
  `msg_is_multiple` int(1) NOT NULL,
  PRIMARY KEY (`conversation_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_conversation_detail` WRITE;
/*!40000 ALTER TABLE `tbl_conversation_detail` DISABLE KEYS */;

INSERT INTO `tbl_conversation_detail` (`conversation_detail_id`, `convo_category_id`, `sender_name`, `receiver_name`, `msg_content`, `msg_datetime`, `msg_is_answered`, `msg_is_bot`, `msg_is_multiple`)
VALUES
	(1,1,'','abel','Hello abel , Im Twice. Is there anything i could help you?','2020-06-06 09:15:39',0,2,0),
	(2,1,'abel','','who is the president of information technology ?','2020-06-06 09:15:46',1,1,0),
	(3,1,'','abel','1','2020-06-06 09:15:46',0,0,1),
	(4,1,'abel','','who is the president ?','2020-06-06 09:23:05',1,1,0),
	(5,1,'','abel','1,2,3','2020-06-06 09:23:05',0,0,2),
	(6,1,'abel','','test','2020-06-06 10:26:01',0,1,0),
	(7,1,'','abel','Your response is beyond my comprehension','2020-06-06 10:26:01',0,0,0),
	(8,1,'abel','','who is the president ?','2020-06-06 10:26:56',1,1,0),
	(9,1,'','abel','1,2,3','2020-06-06 10:26:56',0,0,2),
	(10,2,'','abel','Hello abel , Im Twice. Is there anything i could help you?','2020-06-06 10:47:43',0,2,0),
	(11,2,'abel','','who is the president ?','2020-06-06 10:47:45',1,1,0),
	(12,2,'','abel','1,2,3','2020-06-06 10:47:45',0,0,2),
	(13,2,'abel','','who is the president ?','2020-06-06 10:56:53',1,1,0),
	(14,2,'','abel','1,2,3','2020-06-06 10:56:53',0,0,2),
	(15,2,'abel','','test','2020-06-06 10:57:10',0,1,0),
	(16,2,'','abel','Your response is beyond my comprehension','2020-06-06 10:57:10',0,0,0),
	(17,2,'abel','','who is the president ?','2020-06-06 11:00:33',1,1,0),
	(18,2,'','abel','1,2,3','2020-06-06 11:00:33',0,0,2),
	(19,2,'abel','','who is the president ?','2020-06-06 11:02:50',0,1,0),
	(20,2,'','abel','Your response is beyond my comprehension','2020-06-06 11:02:50',0,0,0),
	(21,2,'abel','','who is the president ?','2020-06-06 11:03:27',0,1,0),
	(22,2,'','abel','Your response is beyond my comprehension','2020-06-06 11:03:27',0,0,0),
	(23,2,'abel','','who is the president ?','2020-06-06 11:04:41',0,1,0),
	(24,2,'','abel','Your response is beyond my comprehension','2020-06-06 11:04:41',0,0,0),
	(25,1,'abel','','who is the president ?','2020-06-06 11:04:56',1,1,0),
	(26,1,'','abel','1,2,3','2020-06-06 11:04:56',0,0,2),
	(27,2,'abel','','who is the president ?','2020-06-06 11:05:00',0,1,0),
	(28,2,'','abel','Your response is beyond my comprehension','2020-06-06 11:05:00',0,0,0),
	(29,3,'','abel','Hello abel , Im Twice. Is there anything i could help you?','2020-06-06 11:05:01',0,2,0),
	(30,3,'abel','','who is the president ?','2020-06-06 11:05:04',0,1,0),
	(31,3,'','abel','Your response is beyond my comprehension','2020-06-06 11:05:04',0,0,0),
	(32,4,'','abel','Hello abel , Im Twice. Is there anything i could help you?','2020-06-06 16:29:43',0,2,0),
	(33,4,'abel','','who is the president','2020-06-06 16:29:57',1,1,0),
	(34,4,'','abel','1,2,3','2020-06-06 16:29:57',0,0,2),
	(35,5,'','abe','Hello abe , Im Twice. Is there anything i could help you?','2020-06-09 10:18:21',0,2,0),
	(36,5,'abe','','President','2020-06-09 10:26:33',1,1,0),
	(37,5,'','abe','1,2,3','2020-06-09 10:26:34',0,0,2),
	(38,5,'abe','','president of information technology','2020-06-09 10:26:57',1,1,0),
	(39,5,'','abe','1','2020-06-09 10:26:57',0,0,1),
	(40,5,'abe','','president of the philippines','2020-06-09 10:27:27',1,1,0),
	(41,5,'','abe','1,2,3','2020-06-09 10:27:27',0,0,2),
	(42,6,'','abe','Hello abe , Im Twice. Is there anything i could help you?','2020-06-09 10:27:36',0,2,0),
	(43,7,'','m','Hello m , Im Twice. Is there anything i could help you?','2020-06-09 10:42:20',0,2,0),
	(44,8,'','o','Hello o , Im Twice. Is there anything i could help you?','2020-06-09 10:44:35',0,2,0),
	(45,8,'o','','who is the president','2020-06-09 10:59:16',1,1,0),
	(46,8,'','o','1,2,3','2020-06-09 10:59:16',0,0,2),
	(47,9,'','o','Hello o , Im Twice. Is there anything i could help you?','2020-06-09 11:08:20',0,2,0),
	(48,10,'','o','Hello o , Im Twice. Is there anything i could help you?','2020-06-09 11:08:21',0,2,0),
	(49,8,'o','','who is the president','2020-06-09 11:15:47',1,1,0),
	(50,8,'','o','1,2,3','2020-06-09 11:15:47',0,0,2),
	(51,8,'o','','who is the president','2020-06-09 11:16:03',1,1,0),
	(52,8,'','o','1,2,3','2020-06-09 11:16:03',0,0,2),
	(53,8,'o','','asdasdas','2020-06-09 11:16:18',0,1,0),
	(54,8,'','o','Your response is beyond my comprehension','2020-06-09 11:16:19',0,0,0),
	(55,8,'o','','who is the president','2020-06-09 11:23:31',1,1,0),
	(56,8,'','o','1,2,3','2020-06-09 11:23:31',0,0,2),
	(57,11,'','KAye','Hello KAye , Im Twice. Is there anything i could help you?','2020-07-27 15:50:27',0,2,0),
	(58,11,'KAye','','jo sallilas','2020-07-27 15:50:50',0,1,0),
	(59,11,'','KAye','Your response is beyond my comprehension','2020-07-27 15:50:51',0,0,0),
	(60,11,'KAye','','president','2020-07-27 15:51:21',1,1,0),
	(61,11,'','KAye','1,2','2020-07-27 15:51:21',0,0,2),
	(62,11,'KAye','','information technology','2020-07-27 15:51:41',1,1,0),
	(63,11,'','KAye','1','2020-07-27 15:51:41',0,0,1),
	(64,12,'','Abel','Hello Abel , Im Twice. Is there anything i could help you?','2020-08-17 16:05:46',0,2,0),
	(65,12,'Abel','','Who is the janitor ?','2020-08-17 16:05:56',0,1,0),
	(66,12,'','Abel','Your response is beyond my comprehension','2020-08-17 16:05:56',0,0,0),
	(67,12,'Abel','','president','2020-08-17 16:06:18',0,1,0),
	(68,12,'','Abel','Your response is beyond my comprehension','2020-08-17 16:06:18',0,0,0);

/*!40000 ALTER TABLE `tbl_conversation_detail` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_conversation_header
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_conversation_header`;

CREATE TABLE `tbl_conversation_header` (
  `conversation_header_id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_name` varchar(50) NOT NULL,
  `date_added` date NOT NULL,
  PRIMARY KEY (`conversation_header_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_conversation_header` WRITE;
/*!40000 ALTER TABLE `tbl_conversation_header` DISABLE KEYS */;

INSERT INTO `tbl_conversation_header` (`conversation_header_id`, `guest_name`, `date_added`)
VALUES
	(1,'abel','2020-06-06'),
	(2,'abel','2020-06-06'),
	(3,'abe','2020-06-09'),
	(4,'m','2020-06-09'),
	(5,'o','2020-06-09'),
	(6,'KAye','2020-07-27'),
	(7,'Abel','2020-08-17');

/*!40000 ALTER TABLE `tbl_conversation_header` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_keywords_answers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_keywords_answers`;

CREATE TABLE `tbl_keywords_answers` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) NOT NULL,
  `keyword_answers` text NOT NULL,
  `additional_info` text NOT NULL,
  PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`user_id`, `username`, `password`)
VALUES
	(1,'admin','a');

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
