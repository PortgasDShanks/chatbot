<?php
include 'core/config.php';
if(isset($_SESSION['user_id'])){
  header("Location: index_1.php");
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>CHATBOT</title>
  <!-- CSS -->
  <link rel="icon" type="image/png" href="assets/images/messages.png">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/login_style.css">
  <link href="assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
  <!-- JS -->
  <script type="text/javascript" src="assets/js/jquery.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.js"></script>
  <style type="text/css">
body{
   
   /* background-image: linear-gradient(to right, rgb(172 212 232),rgb(1 127 175));
    */
    background-position: center;
    background-image: linear-gradient(to right top, #86ee4e, #bde865, #dfe486, #efe2ad, #eee5d6);
    background-repeat: no-repeat;
    background-size: contain;
    background-attachment: fixed;
}
#welcome_container{
  height: 100%;
    background-image: url(assets/images/logo_c.png);
    background-position: center;
    background-repeat: no-repeat;
    /*background-size: contain;
    background-attachment: fixed;*/
}
.navbar-inverse {
    background-color: #2b2b2b00;
    border-color: #132225;
}
b{
  color: #fff;
}
.navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:hover, .navbar-inverse .navbar-nav > .open > a:focus {
    color: #fff;
    background-color: #0000001f;
}
#login-dp{
    min-width: 250px;
    padding: 14px 14px 0;
    overflow: hidden;
    background-color: rgb(33 33 33 / 43%);
    min-height: 200px;
}
#login-dp .help-block{
    font-size:12px    
}
#login-dp .bottom{
    background-color:rgba(255,255,255,.8);
    border-top:1px solid #ddd;
    clear:both;
    padding:14px;
}
#login-dp .form-group {
    margin-bottom: 10px;
}
@media(max-width:768px){
    #login-dp{
        background-color: inherit;
        color: #fff;
    }
    #login-dp .bottom{
        background-color: inherit;
        border-top:0 none;
    }
}
.welcome-message{
    background-color: rgb(51 127 255 / 12%);
    min-height: 300px;
    border-radius: 5px 5px 5px 5px;
    padding: 10px;
}
#circle {
    position: absolute;
    top: 50%;
    left: 45%;
    transform: translate(-50%,-50%);
	width: 200px;
    height: 200px;	
}

.loader {
    width: calc(100% - 0px);
	height: calc(100% - 0px);
	border: 8px solid #162534;
	border-top: 8px solid #09f;
	border-radius: 50%;
	animation: rotate 5s linear infinite;
}

@keyframes rotate {
100% {transform: rotate(360deg);}
}

#wrap2{
  background-color: rgba(33, 33, 33, 0.69);
    min-height: 500px !important;
    border-radius: 5px 5px 5px 5px;
}

/* chat */
.container{max-width:100%; margin:auto;max-height: 500px !important;}
img{ max-width:100%;}
.inbox_people {
  background-color: rgba(33, 33, 33, 0.69);
  float: left;
  overflow: hidden;
  width: 30%; border-right:1px solid #c4c4c4;
}
.inbox_msg {
  border: 1px solid #c4c4c4;
  clear: both;
  overflow: hidden;
  max-height: 480px;
  margin-top: 10px;
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%; padding:
}
.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
.chat_ib h5 span{ font-size:13px; float:right;}
.chat_ib p{ font-size:14px; color:#989898; margin:auto}
.chat_img {
  float: left;
  width: 11%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 88%;
  
}
.chat_list:hover{
    background-color:#9c9c9cb0;
}
.chat_list h5:hover{
    color: white;
}
.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #c4c4c4;
  margin: 0;
  padding: 18px 16px 10px;
  cursor: pointer;
}
.inbox_chat {height: 480px;overflow-y: auto;}

.active_chat{ background:#ebebeb;}

.incoming_msg_img {
  display: inline-block;
  width: 6%;
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 92%;
 }
 .received_withd_msg p {
  background: #ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  color: #646464;
  font-size: 14px;
  margin: 0;
  padding: 5px 10px 5px 12px;
  width: 100%;
}
.time_date {
  color: #747474;
  display: block;
  font-size: 12px;
  margin: 8px 0 0;
}
.received_withd_msg { width: 57%;}
.mesgs {
  /* float: left; */
  padding: 30px 15px 0 25px;
  width: 102%;
}

 .sent_msg p {
  background: #05728f none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 14px;
  margin: 0; color:#fff;
  padding: 5px 10px 5px 12px;
  width:100%;
}
.outgoing_msg{ overflow:hidden; margin: 26px 15px 26px;}
.sent_msg {
  float: right;
  width: 46%;
}
.input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: white;
  font-size: 15px;
  min-height: 48px;
  width: 100%;
  position: absolute;
  left: 0px;
  padding-left: 10px;
}
.input_msg_write{
  width: 100%;
}
.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
.msg_send_btn {
  background: #05728f none repeat scroll 0 0;
  border: medium none;
  border-radius: 50%;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  height: 33px;
  position: relative;
  right: 0;
  top: 9px;
  width: 33px;
  float: right;
  left: 0px;
}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 380px;
  overflow-y: scroll;
  display: block;
}

.spinner {
  /* margin: 100px auto 0;
  width: 70px;
  text-align: center; */
}

.spinner > div {
  width: 18px;
  height: 18px;
  background-color: #333;

  border-radius: 100%;
  display: inline-block;
  -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay 1.4s infinite ease-in-out both;
}

.spinner .bounce1 {
  -webkit-animation-delay: -0.32s;
  animation-delay: -0.32s;
}

.spinner .bounce2 {
  -webkit-animation-delay: -0.16s;
  animation-delay: -0.16s;
}

@-webkit-keyframes sk-bouncedelay {
  0%, 80%, 100% { -webkit-transform: scale(0) }
  40% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bouncedelay {
  0%, 80%, 100% { 
    -webkit-transform: scale(0);
    transform: scale(0);
  } 40% { 
    -webkit-transform: scale(1.0);
    transform: scale(1.0);
  }
}
  </style>
</head>
<body>
  <div id='welcome_container'>
      <nav class="navbar navbar-default navbar-inverse" style="border: none;" role="navigation" style=''>
        <div class="container-fluid">
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b style="color: black;">Login as Admin</b> <span class="caret"></span></a>
            <ul id="login-dp" class="dropdown-menu">
              <li>
                <div class="row">
                  <div class="col-md-12" style='padding-top: 20px;'>
                      <form class="form" role="form" method="post" id="loginForm">
                          <div class="form-group">
                                  <label class="sr-only" for="exampleInputEmail2">Username</label>
                                  <input type="text" class="form-control" name="username" id="exampleInputEmail2" style="background-color:rgba(33, 33, 33, 0.26);color:white;" autocomplete="off" placeholder="Username" required>
                          </div>
                          <div class="form-group">
                                  <label class="sr-only" for="exampleInputPassword2">Password</label>
                                  <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" name="password" style="background-color:rgba(33, 33, 33, 0.26);color:white;" required>
                          </div>
                          <div class="form-group">
                                  <button type="submit" class="btn btn-inverse btn-block" style="background-color:rgba(33, 33, 33, 0.26);color:white;"><span class='fa fa-sign-in'></span> Sign in</button>
                          </div>
                          <div id='response'></div>
                      </form>
                  </div>
                </div>
              </li>
            </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="col-sm-6 col-sm-offset-3" style='' id="wrap">
        <div class='welcome-message'>
            <center> 
                <h1 style='color: #080808;font-weight: bolder;'>Welcome to CHMSC BOT</h1>
                <h5 style='color: #040404;text-align: justify;padding: 20px;font-weight: bolder;font-size: 18px;line-height: 2;'>
                  Hello , This is CHMSC BOT. I'm developed using artificial intellegence algorithms and I am here to answer your questions regarding to Carlos Hilado Memorial State College.
                </h5>
                <div class='input-group'>
                <input type="text" autocomplete='off' class="form-control" id="guestName" placeholder="Enter your name" style="background-color: rgb(255 255 255);color: black;">
                </div>
                <button style="margin-top:10px" onclick="startchat()" class='btn btn-primary btn-sm' id="startBtn"><span class="fa fa-hand-o-right"></span> Get Started</button>
            </center>  
        </div>  

</div>
  </div>
</body>
<script type="text/javascript">
  $(document).ready( function(){
    var guest = $("#guestName").val();
    if(guest == ''){
      $("#startBtn").prop("disabled", true);
    }else{
      $("#startBtn").attr("disabled", false);
    }

    $("#guestName").keyup( function(){
      if($(this).val() == ''){
        $("#startBtn").prop("disabled", true);
      }else{
        $("#startBtn").attr("disabled", false);
      }
    });
    $("#loginForm").submit(function(e){
       e.preventDefault();
       var url = 'ajax/authentication.php';
       var data = $(this).serialize();
       $.post(url , data , function(data){
         if(data == 1){
          window.location = 'index_1.php?page=dashboard';
         }else{
           setTimeout( function(){
            $("#response").html("");
           }, 4000);
          $("#response").html("<h6 style='color: red;text-align:center;' class='animated flash infinite'> Credentials did not match! </h6>");
         }
       });
    });
  });
  function guestname(){
    $("#startBtn").prop("disabled", false);
  }
  function startchat(){
    var guestName = $("#guestName").val();
    $(".welcome-message").html("<div id='circle'><div class='loader'><div class='loader'><div class='loader'><div class='loader'></div></div></div></div></div>");
    $(".welcome-message").css("background-color","rgba(33, 32, 32, 0)");
    $.post("ajax/addGuestConvo.php", {
      guestName: guestName
    }, function(data){
      if(data > 0){
        window.location = 'chat/chat.php?id='+data;
      }else{
        alert("Sorry, Please Try again!");
      }
    });
  }
  function startchat_content(id){
    $.post("views/chatbot.php", {
      id: id
    }, function(data){
      $("#wrap2").html(data);
    });
  }

  $(document).ready( function(){});
    function category_msg(catID){
    
      var headerConvoId = $("#convoID").val();
        $(".msg_history").html("<div id='circle'><div class='loader'><div class='loader'><div class='loader'><div class='loader'></div></div></div></div></div>");
        $.post("ajax/chatbot_message_setup.php", {
            catID: catID,
            convo_id: headerConvoId
        }, function(data){
            if(data == 1){
              // $("#chat_list"+catID).css("background-color","#79797994");
              getMsg(headerConvoId, catID);
            }
        });
    }
    function getMsg(conversationID, categoryType){
      $.post("ajax/chatbot_message_content.php", {
        conversationID: conversationID,
        categoryType: categoryType
      }, function(data){
        var msg = JSON.parse(data);
        $(".type_msg").css("display","block");
        $(".msg_history").html(msg.msg_content);
        updateScroll();
      });
    }
    function sendmsg(conID){
      var message = $("#write_msg").val();
      var categories = $("#categories").val();
      $("#sendMsg").prop("disabled", true);
      $("#sendMsg").html("<span class='fa fa-spin fa-spinner'></span>");
      $.post("ajax/sendMessage.php", {
        conID: conID,
        message: message,
        categories: categories
      }, function(data){
        if(data > 0){
          getMsg(conID, categories);
          botResponse(conID, categories,data);
        }else{
        }
        $("#sendMsg").prop("disabled", false);
        $("#sendMsg").html("<span class='fa fa-paper-plane-o'></span>");
        $("#write_msg").val("");
        updateScroll();
      });
    }
    function botResponse(convoID, catType,latestChat){
     
      $.post("ajax/bot_response.php", {
        convoID: convoID,
        catType: catType,
        latestChat: latestChat
      }, function(data){
        alert(data);
        // if(data > 0){
        //     getMsg(convoID, catType);
        // }
        updateScroll();
      });
    }
    function hideLoader(){
      $("#loader").css("display","none");
    }
    function updateScroll(){
      $(".msg_history").scrollTop($(".msg_history")[0].scrollHeight);
  }

</script>
</html>