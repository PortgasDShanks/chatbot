<?php
$array = array("January" => 1, "February" => 2, "March" => 3, "April" => 4, "May" => 5, "June" => 6, "July" => 7, "August" => 8, "September" => 9, "October" => 10, "November" => 11, "December" => 12);
include '../core/config.php';
$date = date("Y", strtotime(getCurrentDate()));
$header['series'] = array();
$data['name'] = '';
$data['colorByPoint'] = true;
$data['data'] = array();

foreach ($array as $mnth_text => $mnth_num) {
	$list = array();
	$list['name'] = strtoupper($mnth_text);

	$count_user_questions = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_conversation_detail WHERE msg_is_bot = 1 AND MONTH(msg_datetime) = '$mnth_num' AND YEAR(msg_datetime) = '$date'"));

	$count_user_answered_question = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_conversation_detail WHERE msg_is_bot = 1 AND msg_is_answered = 1 AND MONTH(msg_datetime) = '$mnth_num' AND YEAR(msg_datetime) = '$date'"));

	if($count_user_questions[0] == 0 or $count_user_questions[0] == null or $count_user_questions[0] == ''){
		 $total = 0;
	}else{	
		$total = ($count_user_answered_question[0]/$count_user_questions[0]) * 100;
	}

	$list['y'] = round($total);
	array_push($data['data'], $list);
}
array_push($header['series'], $data);
echo json_encode($header);
?>