<?php
include '../core/config.php';
$guestID = $_GET['id'];
?>
<!DOCTYPE html>
<html>
<head>
  <title>CHATBOT</title>
  <!-- CSS -->
  <link rel="icon" type="image/png" href="../assets/images/messages.png">
  <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/login_style.css">
  <link href="../assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="../assets/css/animate.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/confirm.css">
  <!-- JS -->
  
  <style type="text/css">
body{
   
   /* background-image: linear-gradient(to right, rgb(172 212 232),rgb(1 127 175));
    */
    background-position: center;
    background-image: linear-gradient(to right top, #86ee4e, #bde865, #dfe486, #efe2ad, #eee5d6);
    background-repeat: no-repeat;
    background-size: contain;
    background-attachment: fixed;
}
#chat_container{
  height: 100%;
    background-image: url(../assets/images/logo_c.png);
    background-position: center;
    background-repeat: no-repeat;
    /*background-size: contain;
    background-attachment: fixed;*/
}
#wrap2{
  background-color: rgba(33, 33, 33, 0.26);
    min-height: 500px !important;
    border-radius: 5px 5px 5px 5px;
}

/* chat */
.container{max-width:100%; margin:auto;max-height: 500px !important;}
img{ max-width:100%;}
.inbox_people {
  background-color: rgba(33, 33, 33, 0.26);
  float: left;
  overflow: hidden;
  width: 30%; border-right:1px solid #c4c4c4;
}
.inbox_msg {
  border: 1px solid #c4c4c4;
  clear: both;
  overflow: hidden;
  max-height: 480px;
  margin-top: 10px;
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%; padding:
}
.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
.chat_ib h5 span{ font-size:13px; float:right;}
.chat_ib p{ font-size:14px; color:#989898; margin:auto}
.chat_img {
  float: left;
  width: 11%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 88%;
  
}
.chat_list:hover{
    background-color:#9c9c9cb0;
}
.chat_list h5:hover{
    color: white;
}
.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #c4c4c4;
  margin: 0;
  padding: 18px 16px 10px;
  cursor: pointer;
}
.inbox_chat {height: 480px;overflow-y: auto;}

.active_chat{ background:#ebebeb;}

.incoming_msg_img {
  display: inline-block;
  width: 6%;
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 92%;
 }
 .received_withd_msg p {
  background: #ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  color: #646464;
  font-size: 14px;
  margin: 0;
  padding: 5px 10px 5px 12px;
  width: 100%;
}
.time_date {
  color: #ffffff;
  display: block;
  font-size: 12px;
  margin: 8px 0 0;
}
.received_withd_msg { width: 57%;}
.mesgs {
  /* float: left; */
  padding: 30px 15px 0 25px;
  width: 102%;
}

 .sent_msg p {
  background: #05728f none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 14px;
  margin: 0; color:#fff;
  padding: 5px 10px 5px 12px;
  width:100%;
}
.outgoing_msg{ overflow:hidden; margin: 26px 15px 26px;}
.sent_msg {
  float: right;
  width: 46%;
}
.input_msg_write input {
  background: rgb(235 235 235) none repeat scroll 0 0;
  border: medium none;
  color: black;
  font-size: 15px;
  min-height: 48px;
  width: 100%;
  position: absolute;
  left: 0px;
  padding-left: 10px;
}
.input_msg_write{
  width: 100%;
}
.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
.msg_send_btn {
  background: #05728f none repeat scroll 0 0;
  border: medium none;
  border-radius: 50%;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  height: 33px;
  position: relative;
  right: 5px;
  top: 9px;
  width: 33px;
  float: right;
}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 380px;
  overflow-y: scroll;
  display: block;
}

.spinner {
  /* margin: 100px auto 0;
  width: 70px;
  text-align: center; */
}

.spinner > div {
  width: 18px;
  height: 18px;
  background-color: #333;

  border-radius: 100%;
  display: inline-block;
  -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  animation: sk-bouncedelay 1.4s infinite ease-in-out both;
}

.spinner .bounce1 {
  -webkit-animation-delay: -0.32s;
  animation-delay: -0.32s;
}

.spinner .bounce2 {
  -webkit-animation-delay: -0.16s;
  animation-delay: -0.16s;
}

@-webkit-keyframes sk-bouncedelay {
  0%, 80%, 100% { -webkit-transform: scale(0) }
  40% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bouncedelay {
  0%, 80%, 100% { 
    -webkit-transform: scale(0);
    transform: scale(0);
  } 40% { 
    -webkit-transform: scale(1.0);
    transform: scale(1.0);
  }
}

.loading:after {
  content: ' .';
  animation: dots 1s steps(5, end) infinite;}
@keyframes dots {
  0%, 20% {
    color: rgba(0,0,0,0);
    text-shadow:
      .25em 0 0 rgba(0,0,0,0),
      .5em 0 0 rgba(0,0,0,0);}
  40% {
    color: white;
    text-shadow:
      .25em 0 0 rgba(0,0,0,0),
      .5em 0 0 rgba(0,0,0,0);}
  60% {
    text-shadow:
      .25em 0 0 white,
      .5em 0 0 rgba(0,0,0,0);}
  80%, 100% {
    text-shadow:
      .25em 0 0 white,
      .5em 0 0 white;}}
  </style>
</head>
<body>
  <div id='chat_container'>
      <div class='col-sm-8 col-sm-offset-2' style='top:20px;z-index: 2;'>

<button class='btn btn-sm btn-inverse pull-right' onclick="getFAQ()" style='background-color:rgba(33, 33, 33, 0.26);color:white;'><span class='fa fa-question-circle'></span> FAQ</button>

<button class='btn btn-sm btn-inverse pull-right' onclick="window.location='../index.php'" style='background-color:rgba(33, 33, 33, 0.26);color:white;'><span class='fa fa-arrow-left'></span> Back</button>

</div>
<div class="col-sm-8 col-sm-offset-2" id="wrap2" style='top:40px;z-index: 2;'>

        <div class="row">
        
          <div class="container" >
         
          <div class="messaging">
              <div class="inbox_msg">
                  <div class="inbox_people">
                  <div class="headind_srch">
                      <div class="recent_heading">
                      <h4 style="color: #ffffff;font-weight: 900;">Categories</h4>
                      </div>
                  </div>
                  <div class="inbox_chat">
                  <?php 
                      $cat = mysql_query("SELECT * FROM tbl_chat_categories")or die(mysql_error());
                      while($row = mysql_fetch_array($cat)){
                          
                  ?>
                      <div class="chat_list" id='chat_list<?php echo $row['category_id']?>'>
                      <div class="chat_people">
                          <div class="chat_ib" onclick="category_msg('<?php echo $row['category_id']?>')">
                          <h5 style='color: white;'><?php echo $row['category_name']?></h5>
                          </div>
                      </div>
                      </div>
                  <?php } ?>
                  </div>
                  </div>
                  <div class="mesgs">
                      <div class="msg_history"><h5 style='color: white !important;padding-left: 10px;font-weight: bolder;font-size: 18px;'>Please Choose any category to start conversation</h5></div>
                      <!-- <div id="loader" style='color:white'>Loader</div> -->
                  </div>
                  <div class="type_msg" style="display: none;margin-top:15px;margin-top: 12px;width: 68%;left:31%;">
                          <div class="input_msg_write">
                          <input type="text" class="write_msg" id="write_msg" autofocus placeholder="Type a message" />
                          <input type="hidden" id="msg_categ"/>
                          <button class="msg_send_btn" id='sendMsg' type="button" onclick="sendmsg('<?php echo $guestID; ?>')"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                          </div>
                      </div>
              </div>
          </div>
      </div>

</div>
</div>
  </div>
<!-- <div id="particles-js"></div>  -->
<div class="modal fade" id="faqModal" tabindex="-1"> 
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><span class='fa fa-question-circle'></span>  FREQUENTLY ASK QUESTION </h4>
        </div>
      <div class="modal-body">
         <div id='FAQ'>
           
         </div>
      </div>
      <div class="modal-footer">
          <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
      </div>
    </div>
  </div>
</div>
</body>
<script type="text/javascript" src="../assets/js/jquery.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap.js"></script>
<script src="../assets/js/confirm.js"></script>
<script type="text/javascript">
  $(document).ready( function(){});
  function getFAQ(){
    $("#faqModal").modal();
    faqList();
  }
  function faqList(){
    $.post('faq_list.php',{

    }, function(data){
        $("#FAQ").html(data);
    })
  }
    function category_msg(catID){
        var guest = '<?php echo $guestID ?>';
        $(".msg_history").html("<div id='circle'><div class='loader'><div class='loader'><div class='loader'><div class='loader'></div></div></div></div></div>");
        $.post("../ajax/chatbot_message_setup.php", {
            catID: catID,
            guest: guest
        }, function(data){
           // alert(data);
            if(data == 1){
              getMsg(guest, catID);

            }
        });
    }
    function getMsg(conversationID, categoryType){
      $.post("../ajax/chatbot_message_content.php", {
        conversationID: conversationID,
        categoryType: categoryType
      }, function(data){
        var msg = JSON.parse(data);
        $(".type_msg").css("display","block");
        $(".msg_history").html(msg.msg_content);
        $("#msg_categ").val(categoryType);
        updateScroll();
      });
    }
    function sendmsg(conID){
      var message = $("#write_msg").val();
      var categories = $("#msg_categ").val();
      //alert(categories);
      $("#sendMsg").prop("disabled", true);
      $("#sendMsg").html("<span class='fa fa-spin fa-spinner'></span>");
      $.post("../ajax/sendMessage.php", {
        conID: conID,
        message: message,
        categories: categories
      }, function(data){
        if(data > 0){
          getMsg(conID, categories);
          botResponse(conID, categories,data);
        }else{
        }
        $("#sendMsg").prop("disabled", false);
        $("#sendMsg").html("<span class='fa fa-paper-plane-o'></span>");
        $("#write_msg").val("");
        updateScroll();
      });
    }
    function botResponse(convoID, catType,latestChat){
      // setTimeout(function(){
      //   typing_bot(latestChat);
      // }, 5000);
      
      $.post("../ajax/bot_response.php", {
        convoID: convoID,
        catType: catType,
        latestChat: latestChat
      }, function(data){
        //alert(data);
        if(data > 0){
            getMsg(convoID, catType);
        }
        updateScroll();
      });
    }
    function showInfo(rowID){
      $.post("../ajax/getAdditionalInfo.php", {
        rowID: rowID
      }, function(data){
        if(data != ''){
          info_popup(data);
        }
      });
    }
    // function typing_bot(latestChat){
    //   $("#received"+latestChat).html("<p class='loading'></p>");
    // }
    function info_popup(addInfo){
      $.confirm({
          columnClass: 'col-md-6 col-md-offset-3',
          icon: 'fa fa-info-circle',
          title: "Information",
          content: addInfo,
          backgroundDismiss: false,
          backgroundDismissAnimation: 'glow',
          closeIcon: true,
          buttons:{
            Close:{}
          }
      });
    }
    function hideLoader(){
      $("#loader").css("display","none");
    }
    function updateScroll(){
      $(".msg_history").scrollTop($(".msg_history")[0].scrollHeight);
  }

   // $.getScript("../assets/js/particles.min.js", function(){
   //  particlesJS('particles-js',
   //    {
   //      "particles": {
   //        "number": {
   //          "value": 80,
   //          "density": {
   //            "enable": true,
   //            "value_area": 800
   //          }
   //        },
   //        "color": {
   //          "value": "#ffffff"
   //        },
   //        "shape": {
   //          "type": "circle",
   //          "stroke": {
   //            "width": 0,
   //            "color": "#000000"
   //          },
   //          "polygon": {
   //            "nb_sides": 5
   //          },
   //          "image": {
   //            "width": 100,
   //            "height": 100
   //          }
   //        },
   //        "opacity": {
   //          "value": 0.5,
   //          "random": false,
   //          "anim": {
   //            "enable": false,
   //            "speed": 1,
   //            "opacity_min": 0.1,
   //            "sync": false
   //          }
   //        },
   //        "size": {
   //          "value": 5,
   //          "random": true,
   //          "anim": {
   //            "enable": false,
   //            "speed": 40,
   //            "size_min": 0.1,
   //            "sync": false
   //          }
   //        },
   //        "line_linked": {
   //          "enable": true,
   //          "distance": 150,
   //          "color": "#ffffff",
   //          "opacity": 0.4,
   //          "width": 1
   //        },
   //        "move": {
   //          "enable": true,
   //          "speed": 6,
   //          "direction": "none",
   //          "random": false,
   //          "straight": false,
   //          "out_mode": "out",
   //          "attract": {
   //            "enable": false,
   //            "rotateX": 600,
   //            "rotateY": 1200
   //          }
   //        }
   //      },
   //      "interactivity": {
   //        "detect_on": "canvas",
   //        "events": {
   //          "onhover": {
   //            "enable": true,
   //            "mode": "repulse"
   //          },
   //          "onclick": {
   //            "enable": true,
   //            "mode": "push"
   //          },
   //          "resize": true
   //        },
   //        "modes": {
   //          "grab": {
   //            "distance": 400,
   //            "line_linked": {
   //              "opacity": 1
   //            }
   //          },
   //          "bubble": {
   //            "distance": 400,
   //            "size": 40,
   //            "duration": 2,
   //            "opacity": 8,
   //            "speed": 3
   //          },
   //          "repulse": {
   //            "distance": 200
   //          },
   //          "push": {
   //            "particles_nb": 4
   //          },
   //          "remove": {
   //            "particles_nb": 2
   //          }
   //        }
   //      },
   //      "retina_detect": true,
   //      "config_demo": {
   //        "hide_card": false,
   //        "background_color": "#b61924",
   //        "background_image": "",
   //        "background_position": "50% 50%",
   //        "background_repeat": "no-repeat",
   //        "background_size": "cover"
   //      }
   //    }
   //  )
   //  });

</script>
</html>