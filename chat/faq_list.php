<?php
include("../core/config.php");

$title_words   = array();
$content_words = array();


$regex = '/[^A-Z]/';

$sql = "SELECT msg_content FROM tbl_conversation_detail WHERE msg_is_bot = 1";
$res = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($res)){
	$title  = preg_replace($regex, ' ', strtoupper($row["msg_content"]));
    $title  = preg_replace('/\s\s+/', ' ', $title);

    $words  = explode(' ', $title);
    
   
    foreach($words as $word)
    {
        if (!isset($title_array[$word])) $title_array[$word] = 0;
        $title_array[$word]++;
    }
}

krsort($title_array);


$limit = 10;
echo "<ol>";
foreach ($title_array as $word => $count)
{
	echo "<li> $word" . PHP_EOL."</li>";
    $limit--;
    if (!$limit) break;
}
echo "</ol>";