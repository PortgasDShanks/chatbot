<?php
include 'core/config.php';

$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>CHATBOT</title>
  <link rel="icon" type="image/png" href="assets/images/messages.png">
  <!-- All CSS -->
  <link rel="stylesheet" href="assets/css/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/adminlte.min.css">
  <link rel="stylesheet" href="assets/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/myStyle.css">
  <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css">
  <link rel="stylesheet" href="assets/css/dataTables.bootstrap4.css">
  <link href='assets/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
  <link href='assets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-datepicker.css">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="assets/css/daterangepicker-bs3.css">
  <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
  <link rel="stylesheet" type="text/css" href="assets/css/fastselect.css">
  <link rel="stylesheet" type="text/css" href="assets/css/fSelect.css">
  <!-- All JS -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.bundle.min.js"></script>
  <script src="assets/js/adminlte.min.js"></script>
  <script src="assets/js/sweetalert.min.js"></script>
  <script src="assets/js/jquery.dataTables.js"></script>
  <script src="assets/js/dataTables.bootstrap4.js"></script>
  <script src='assets/fullcalendar/lib/moment.min.js'></script>
  <script src="assets/fullcalendar/fullcalendar.min.js"></script>
  <script type="text/javascript" src="assets/js/datepicker.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap-timepicker.min.js"></script>
  <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-ui.js"></script>
  <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
  <script type="text/javascript" src="assets/js/jquery.knob.js"></script>
  <!-- <script type="text/javascript" src="assets/js/fastselect.min.js"></script> -->
  <script type="text/javascript" src="assets/js/fastselect.standalone.min.js"></script>
  <script type="text/javascript" src="assets/js/fSelect.js"></script>
</head>
<body class="">
<div class="">
  <div class="">
    <?php
    include 'routes/routes.php';
    ?>
  </div>
</div>
</body>
<script type="text/javascript">
  function logout(){
    window.location.href = 'ajax/logout.php';
  }
  function userProfile(){
    window.location = 'index.php?page=profile';
  }
  function add_user(){
    window.location = 'index.php?page=add-users';
  }
  function add_member(){
    window.location = 'index.php?page=add-member';
  }
  function updateAlert(){
    swal({
        title: "All Good!",
        text: "Your data was successfully updated",
        type: "success"
      }, function(){
        location.reload();
      });
  }
  function deleteAlert(){
    swal({
        title: "All Good!",
        text: "Your data was successfully deleted.",
        type: "success"
      }, function(){
        location.reload();
      });
  }
  function insertAlert(){
    swal({
        title: "All Good!",
        text: "Your data was successfully inserted.",
        type: "success"
      }, function(){
        location.reload();
      });
  }
  function failedAlert(){
    swal({
        title: "Aw Snap!",
        text: "Unable to execute the query.",
        type: "error"
      }, function(){
        location.reload();
      });
  }
  function alertWarning(){
    swal("Aw Snap!", "There's something missing in your data.","warning");
  }
  $(document).ready( function(){
    $("#datepicker").datepicker();
    $('#daterange').daterangepicker();
    $('#multipleSelect').fastselect();

    $("#icon_hover").hover( function(){
      $("#icon_hover").removeClass("fa fa-star");
      $("#icon_hover").addClass("fa fa-star-o");
    }, function(){
      $("#icon_hover").removeClass("fa fa-star-o");
      $("#icon_hover").addClass("fa fa-star");
    }); 

    $("#users").fSelect({
      width: '100%',
      placeholder: 'Please Select Users'
    });
  });

</script>
</html>
