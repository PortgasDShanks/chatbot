<div class="modal fade" id="answer" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Keyword Answer </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
               
              </div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Keywords:</strong></span>
                        </div>
						<input type='text' class='form-control' id="keywords" data-role="tagsinput">
						<input type='hidden' id="keyID">
                    </div>
				</div>
				<div class='col-md-12' style='margin-top:10px'>
					<div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Answer:</strong></span>
                        </div>
                        <textarea style='resize:none' id='answer' row='7' class='form-control'></textarea>
                    </div> 
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
                    <button class="btn btn-primary btn-sm" id="btn_up" onclick="updateAnswer()"><span class="fa fa-check"></span> Save Changes</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close
					</button>
				</span>
			</div>
		</div>
	</div>
</div>