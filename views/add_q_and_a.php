<?php 
$chatID = $_REQUEST['chat_id'];
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="index_1.php?page=QandA">Keywords</a></li>
          <li class="breadcrumb-item active">Add Keywords</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              K E Y W O R D S
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <?php 
                if($chatID == ''){
            ?>
                <div class='row'>
                    <div class='col-sm-5'>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Category:</strong></span>
                            </div>
                            <select id='category_type' class='form-control'>
                            <option value="">&mdash; Please Choose &mdash;</option>
                                <?php 
                                $cat = mysql_query("SELECT * FROM tbl_chat_categories");
                                while($row = mysql_fetch_array($cat)){
                                    $rowID = $row['category_id'];
                                ?>
                                    <option value="<?php echo $rowID?>"><?php echo $row['category_name'];?></option>
                            <?php }
                                ?>
                            </select>
                        
                        </div>
                    </div>
                <div class='col-sm-7'>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Keywords:</strong></span>
                        </div>
                        <input type='text' class='form-control' id="keywords" data-role="tagsinput">
                    </div>
                </div>
                <div class='col-sm-6' style='margin-top: 20px;'>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Answer:</strong></span>
                        </div>
                        <textarea style='resize:none' id='answer' row='7' class='form-control'></textarea>
                    </div>  
                </div>
                <div class='col-sm-6' style='margin-top: 20px;'>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Additional Info (Optional):</strong></span>
                        </div>
                        <textarea style='resize:none' id='add_info' row='7' class='form-control'></textarea>
                    </div>  
                </div>
                </div>
                <?php } else { 
                    mysql_query("UPDATE tbl_chat_notifications SET notification_status = 1 WHERE chat_id = '$chatID'");
                    
                    $getChat = mysql_fetch_array(mysql_query("SELECT * FROM tbl_conversation_detail WHERE conversation_detail_id = '$chatID'"));

                    $categ_id = $getChat['convo_category_id'];
                    $keywords_chat = getKeywords_from_msg($chatID);
                    $implode = implode(",", $keywords_chat);
                    $getCategory = mysql_fetch_array(mysql_query("SELECT category_id FROM tbl_conversation_categories WHERE convo_category_id = '$categ_id'"));
                   
                ?>
                <div class='row'>
                    <div class='col-sm-12'><h6 class='alert alert-info'>Question: <?php echo $getChat['msg_content']?></h6></div>
                    <div class='col-sm-4'>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Category:</strong></span>
                            </div>
                            <select id='category_type' class='form-control' readonly>
                                <?php 
                                $cat = mysql_query("SELECT * FROM tbl_chat_categories");
                                while($row = mysql_fetch_array($cat)){
                                    $rowID = $row['category_id'];
                                    $selected = ($rowID == $getCategory[0])?"selected":"";
                                ?>
                                    <option <?php echo $selected; ?> value="<?php echo $rowID?>"><?php echo $row['category_name'];?></option>
                            <?php }
                                ?>
                            </select>
                        
                        </div>
                    </div>
                    <div class='col-sm-8'>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>Keywords:</strong></span>
                            </div>
                            <input type='text' class='form-control' value="<?php echo $implode?>" id="keywords" data-role="tagsinput">
                        </div>
                    </div> 
                    <div class='col-sm-6' style='margin-top: 20px;'>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Answer:</strong></span>
                            </div>
                            <textarea style='resize:none' id='answer' row='7' class='form-control'></textarea>
                        </div>  
                    </div>  
                    <div class='col-sm-6' style='margin-top: 20px;'>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Additional Info (Optional):</strong></span>
                            </div>
                            <textarea style='resize:none' id='add_info' row='7' class='form-control'></textarea>
                        </div>  
                    </div> 
                </div> 
                <?php } ?>
                <div class='col-sm-12' style='margin-top: 20px;'>
                <button class="btn btn-primary btn-sm pull-right" id='btn_save' onclick="addNewKeywords()"><span class="fa fa-check-circle"></span> Save Changes</button>
                </div>    
            </div>
          </div>
        </div>
        </div>
    </div>
</div>
<script>
$(document).ready( function(){
    $("#keywords").tagsinput();
});
function addNewKeywords(){
    var keywords = $("#keywords").val();
    var category = $("#category_type").val();
    var ans = $("#answer").val();
    var additional = $("#add_info").val();
    if(keywords == '' || category == '' || ans == ''){
        swal({
            title: "Aw Snap!",
            text: "Please Fill all fields.",
            type: "warning"
        });
    }else{
        $("#btn_save").prop("disabled", true);
        $("#btn_save").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
        $.post("ajax/add_keywords.php", {
            keywords: keywords,
            category: category,
            ans: ans,
            additional: additional
        }, function(data){
            if(data == 1){
                customAlert("All Good! Keywords was successfully added","success");
            }else{
                customAlert("Aw Snap! Unable to finish your action, Please try again","danger");
            }

            $("#btn_save").prop("disabled", false);
            $("#btn_save").html("<span class='fa fa-check-circle'></span> Save Changes");
            $("#category_type").val("");
            $("#keywords").val("");
            $("#answer").val("");
        });
    }
}
</script>