<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active">Home</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-sm-8 col-md-6">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fa fa-cog"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Today's No. Of Users</span>
              <span class="info-box-number">
                <?php echo CHATBOTUSERS('today') ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-12 col-sm-8 col-md-6">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fa fa-cog"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Overall Users</span>
              <span class="info-box-number">
                <?php echo CHATBOTUSERS('overall'); ?>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-12 col-sm-8 col-md-12">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fa fa-cog"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">BOT Response Rate</span>
               <div class="card-body">
                    <div id="bot_chart"></div>
                  </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
  </div>
</div>

<script src="assets/js/highcharts1.js"></script>
<script src="assets/js/highcharts-more.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    bot_chart();
  });
  function bot_chart(){

  $("#bot_chart").html("<h4><center><span class='fa fa-spinner fa-spin'></span> Loading graph ...</center></h4>");

  $.getJSON('ajax/bot_chart.php', function(data){
//     Highcharts.chart('bot_chart', {

//     title: {
//         text: 'The graphs connect from April to June, despite the null value in May'
//     },

//     xAxis: {
//         categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
//             'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
//     },

//     plotOptions: {
//         series: {
//             connectNulls: true
//         }
//     },

//     series: [{
//         data: [
//             [20, 50],
//             [60, 90],
//             [90, 120],
//             [110, 140],
//             null,
//             [160, 190],
//             [120, 150],
//             [130, 160],
//             [200, 230],
//             [180, 210],
//             [80, 110],
//             [40, 70]
//         ],
//         type: 'arearange',
//         name: 'Area range series',
//         marker: {
//             enabled: false
//         }
//     }, {
//         data: [29.9, 71.5, 106.4, 129.2, null, 176.0, 135.6, 148.5, 216.4,
//             194.1, 95.6, 54.4],
//         name: 'Line series'
//     }]

// });


    Highcharts.chart("bot_chart", {
      chart: {
        type: 'line'
      },
      title: {
        text: "CHMSC-A BOT Response Rate"
      },
      xAxis: {
        type: 'category',
        title: {
          text: 'Date'
        }
      },
      yAxis: {
        title: {
          text: 'Percentage(%)'
        }
      },
      plotOptions: {
          line: {
            dataLabels: {
              enabled: true
            },
            enableMouseTracking: true
          },
          series: {
            connectNulls: true
          }
      },

      tooltip: {
          headerFormat: '',
          pointFormat: '<span style="color:{point.color}">{point.name} </span> <span style="font-size:11px">{series.name}</span>: <b>{point.y:.0f}%</b><br/>'
      },

     series: data['series']
    });

  });
}
</script>
