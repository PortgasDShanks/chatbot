<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Categories</a></li>
          <li class="breadcrumb-item active">Home</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <button class="btn btn-primary btn-sm pull-right" onclick="addCategory()"><span class="fa fa-plus-circle"></span> Add</button>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="cat_table" class="table table-bordered table-hover">
                <thead>
                <tr style='background-color: #343940;color: white;'>
                  <th style='width: 30px;'>#</th>
                  <th style='width: 170px;'>ACTION</th>
                  <th>CATEGORY NAME</th>
                </tr>
                </thead>
                <tbody>
                    
                </tbody>
              </table>
            </div>
          </div>
        </div>
        </div>
    </div>
</div>
<?php require 'Modals/add_category_modal.php'; ?>
<script type="text/javascript">
    function addCategory(){
        $("#addCat").modal('show');
    }
    function addCategories(){
        var catName = $("#Cname").val();
        $("#btn_add_cat").prop("disabled", true);
        $("#btn_add_cat").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
        $.post("ajax/addCategories.php", {
            catName: catName
        }, function(data){
            $("#Cname").val("");
            $("#addCat").modal('hide');
            if(data == 1){
              customAlert("All Good! Category was successfully added","success");
            }else{
              customAlert("Aw Snap! Unable to finish your action, Please try again","danger");
            }
            category_data();
            $("#btn_add_cat").prop("disabled", false);
            $("#btn_add_cat").html("<span class='fa fa-check'></span> Continue");
        })
    }
    function update_category(rowID){
      var catName = $("#catName"+rowID).val();
      $("#updateCategory"+rowID).prop("disabled", true);
      $("#updateCategory"+rowID).html("<span class='fa fa-spin fa-spinner'></span> Loading...");
      $.post("ajax/updateCategory.php", {
        rowID: rowID,
        catName: catName
      }, function(data){
        if(data > 0){
          customAlert("All Good! Category was successfully updated","success");
        }else{
          customAlert("Aw Snap! Unable to finish your action, Please try again","danger");
        }
        category_data();
      });
    }
    function delete_category(rowID){
      $("#deleteCategory"+rowID).prop("disabled", true);
      $("#deleteCategory"+rowID).html("<span class='fa fa-spin fa-spinner'></span> Loading...");
        $.confirm({
          icon: 'fa fa-trash-o',
          title: "Delete Category",
          content: "Are you sure you want to delete this selected categories?",
          backgroundDismiss: false,
          backgroundDismissAnimation: 'glow',
          buttons: {
              No: function () {
                $("#deleteCategory"+rowID).prop("disabled", false);
                $("#deleteCategory"+rowID).html("<span class='fa fa-trash-o'></span> Delete");
              },
              Delete: {
                  text: 'Yes',
                  action: function () {
                    DELETESELECTEDCATEGORY(rowID)
                  }
              }
          }
      });
      
    }
    function DELETESELECTEDCATEGORY(rowID){
      $("#btn-delete-cat").prop("disabled", true);
      $("#btn-delete-cat").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
      $.post("ajax/deleteBulkCategories.php", {
        rowID: rowID
      }, function(data){
          customAlert("All Good! Category was successfully deleted","success");
          category_data();
          $("#btn-delete-cat").prop("disabled", false);
          $("#btn-delete-cat").html("<span class='fa fa-trash-o'></span> Delete");
      });
    }
    function category_data(){
        $("#cat_table").DataTable().destroy();
        $('#cat_table').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/category_list.php",
            "dataSrc":"data"
        },
        "columns":[
            {
            "data":"count"
            },
            {
              "data":"action"
            },
            {
            "data":"Cname"
            }
            
        ]   
        });
    }
    $(document).ready( function(){
        category_data();
    });
</script>
