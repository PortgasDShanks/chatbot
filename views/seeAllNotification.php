<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Notifications</a></li>
          <li class="breadcrumb-item active">Home</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              A L L   N O T I F I C A T I O N S
            </div>
            <!-- /.card-header -->
            <div class="card-body">
               <table id="notif_table" class="table table-bordered table-hover">
                <thead>
                <tr style='background-color: #343940;color: white;'>
                  <th>GUEST</th>
                  <th>QUESTION</th>
                  <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                    
                </tbody>
              </table>
            </div>
          </div>
        </div>
        </div>
    </div>
</div>
<script>
$(document).ready( function(){
  getNotifs();
});
function gotoAdd(chatid){
  window.location='index_1.php?page=add-keywords&chat_id='+chatid;
}
function gotoDelete(notifId){
  $.post("ajax/deleteNotif.php",{
    notifId: notifId
  }, function(data){
    if(data == 1){
      swal({
          title: "All Good!",
          text: "Notification was successfully Deleted.",
          type: "success"
      }, function(){
        getNotifs();
      });
    }else{
      failedAlert();
    }
  });
}
function getNotifs(){
  $("#notif_table").DataTable().destroy();
        $('#notif_table').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/notif_list.php",
            "dataSrc":"data"
        },
        "columns":[
            {
              "data":"guestName"
            },
            {
              "data":"question"
            },
            {
              "data":"action"
            }
            
        ]   
        });
}
</script>
