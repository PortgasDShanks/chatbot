<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Q and A</a></li>
          <li class="breadcrumb-item active">Home</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <button class="btn btn-danger btn-sm pull-right" onclick="deleteKeywords()" id="btn-delete-key" ><span class="fa fa-trash-o"></span> Delete</button>
              <button class="btn btn-primary btn-sm pull-right" onclick="window.location='index_1.php?page=add-keywords&chat_id='"><span class="fa fa-plus-circle"></span> Add</button>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
             <div class='col-md-4' style='margin-bottom: 10px;'>
             <div class="input-group">
                <div class="input-group-prepend">
                <span class="input-group-text"><strong>Category:</strong></span>
                </div>
                <select id='category_type' class='form-control' onchange='categoryType()'>
                    <?php 
                    $cat = mysql_query("SELECT * FROM tbl_chat_categories");
                    while($row = mysql_fetch_array($cat)){
                        $rowID = $row['category_id'];
                    ?>
                        <option value="<?php echo $rowID?>"><?php echo $row['category_name'];?></option>
                    <?php }
                    ?>
                </select>
            </div> 
            </div>
            <hr> 
              <table id="keys_table" class="table table-bordered table-hover" style='margin-top:10px'>
                <thead>
                <tr style='background-color: #343940;color: white;'>
                  <th>#</th>
                  <th id='btns' style='width:55px'></th>
                  <th>KEYWORDS</th>
                  <th>ANSWER</th>
                  <th>ADDITIONAL INFO</th>
                </tr>
                </thead>
                <tbody>
                    
                </tbody>
              </table>
            </div>
          </div>
        </div>
        </div>
    </div>
</div>
<?php require 'Modals/view_answer_modal.php'; ?>
<script>
$(document).ready( function(){
  var catType = $("#category_type").val();
  categoryKeywords(catType);
  $('[data-toggle="tooltip"]').tooltip();
});
function delete_key(rowID){
  $("#delete_key"+rowID).prop("disabled", true);
  $("#delete_key"+rowID).html("<span class='fa fa-spin fa-spinner'></span>");
  $.confirm({
    icon: 'fa fa-trash-o',
    title: "Delete Category",
    content: "Are you sure you want to delete this selected Keyword?",
    backgroundDismiss: false,
    backgroundDismissAnimation: 'glow',
    buttons: {
        No: function () {
          $("#delete_key"+rowID).prop("disabled", false);
          $("#delete_key"+rowID).html("<span class='fa fa-trash-o'></span>");
        },
        Delete: {
            text: 'Yes',
            action: function () {
              deleteKeywords(rowID)
            }
        }
    }
});
}
function deleteKeywords(rowID){
  var catType = $("#category_type").val();
  $.post("ajax/deleteBulkKeywords.php", {
    rowID: rowID,
    catType:catType
  }, function(data){
    if(data > 0){
      customAlert("All Good! Answer was successfully deleted","success");
    }else{
      customAlert("Aw Snap! Unable to finish your action, Please try again","danger");
    }

    categoryKeywords(catType);
  });    
}
function updateMe(){
  if($("#checkUpdate").prop("checked")){
    $("#answer_key").attr("readonly", false);
  }else{
    $("#answer_key").attr("readonly", true);
  }
  
}
function update_answer(rowID){
  var ctype = $("#category_type").val();
  var answer = $("#kAns"+rowID).val();
  var kAddInfo = $("#kAddInfo"+rowID).val();
  $("#update_answer"+rowID).prop("disabled", true);
  $("#update_answer"+rowID).html("<span class='fa fa-spin fa-spinner'></span>");
  $.post("ajax/updateKeywordAnswer.php", {
    rowID: rowID,
    answer: answer,
    kAddInfo: kAddInfo
  }, function(data){
    if(data == 1){
      customAlert("All Good! Answer was successfully updated","success");
    }else{
      customAlert("Aw Snap! Unable to finish your action, Please try again","danger");
    }
    categoryKeywords(ctype);
  });
}
function viewRecord(Kid){
  $("#answer").modal("show");
  $.post("ajax/keywordAnswer.php", {
    Kid: Kid
  }, function(data){
    var a = JSON.parse(data);
    $("#keywords").html(a.keyword_lists);
  });
}
function categoryType(){
  var catType = $("#category_type").val();

  categoryKeywords(catType);
}
function categoryKeywords(catType){
  $("#keys_table").DataTable().destroy();
  $('#keys_table').dataTable({
  "processing":true,
  "ajax":{
      "url":"ajax/datatables/keywords.php",
      "dataSrc":"data",
      "data":{
        catType: catType
      },
      "type":"POST"
  },
  "columns":[
      {
        "data":"count"
      },
      {
        "data":"action"
      },
      {
       "data":"Kname"
      },
      {
        "data":"answer"
      },
      {
        "data":"additional_info"
      }
      
  ]   
  });
}
</script>